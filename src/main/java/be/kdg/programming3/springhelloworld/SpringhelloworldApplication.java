package be.kdg.programming3.springhelloworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SpringhelloworldApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context =
        SpringApplication.run(KnightConfiguration.class, args);
        context.getBean(Knight.class).embarkOnQuest();
        context.close();
    }

}
