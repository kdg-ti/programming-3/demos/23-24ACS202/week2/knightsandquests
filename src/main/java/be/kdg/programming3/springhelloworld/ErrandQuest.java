package be.kdg.programming3.springhelloworld;

public class ErrandQuest implements Quest{
    @Override
    public void embark() {
        System.out.println("Doing these errands...");
    }
}
