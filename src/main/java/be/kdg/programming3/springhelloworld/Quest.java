package be.kdg.programming3.springhelloworld;

public interface Quest {
    void embark();
}
