package be.kdg.programming3.springhelloworld;

public interface Knight {
    void embarkOnQuest();
}
