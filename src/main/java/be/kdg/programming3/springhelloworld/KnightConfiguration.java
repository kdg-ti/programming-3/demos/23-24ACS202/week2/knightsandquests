package be.kdg.programming3.springhelloworld;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KnightConfiguration {
    @Bean
    public Quest quest() {
        return new ErrandQuest();
    }

    @Bean
    public Knight knight(Quest quest){
        return new HandyKnight(quest);
    }
}
